#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "classifier-reborn"
  s.version = "2.2.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Lucas Carlson", "Parker Moore", "Chase Gilliam"]
  s.date = "2017-12-15"
  s.email = ["lucas@rufy.com", "parkrmoore@gmail.com", "chase.gilliam@gmail.com"]
  s.extra_rdoc_files = ["LICENSE", "README.markdown"]
  s.files = ["LICENSE", "README.markdown", "data/stopwords/ar", "data/stopwords/bn", "data/stopwords/ca", "data/stopwords/cs", "data/stopwords/da", "data/stopwords/de", "data/stopwords/en", "data/stopwords/es", "data/stopwords/fi", "data/stopwords/fr", "data/stopwords/hi", "data/stopwords/hu", "data/stopwords/it", "data/stopwords/ja", "data/stopwords/nl", "data/stopwords/no", "data/stopwords/pl", "data/stopwords/pt", "data/stopwords/ru", "data/stopwords/se", "data/stopwords/tr", "data/stopwords/vi", "data/stopwords/zh", "lib/classifier-reborn.rb", "lib/classifier-reborn/backends/bayes_memory_backend.rb", "lib/classifier-reborn/backends/bayes_redis_backend.rb", "lib/classifier-reborn/backends/no_redis_error.rb", "lib/classifier-reborn/bayes.rb", "lib/classifier-reborn/category_namer.rb", "lib/classifier-reborn/extensions/hasher.rb", "lib/classifier-reborn/extensions/vector.rb", "lib/classifier-reborn/extensions/vector_serialize.rb", "lib/classifier-reborn/lsi.rb", "lib/classifier-reborn/lsi/cached_content_node.rb", "lib/classifier-reborn/lsi/content_node.rb", "lib/classifier-reborn/lsi/summarizer.rb", "lib/classifier-reborn/lsi/word_list.rb", "lib/classifier-reborn/validators/classifier_validator.rb", "lib/classifier-reborn/version.rb"]
  s.homepage = "https://github.com/jekyll/classifier-reborn"
  s.licenses = ["LGPL"]
  s.rdoc_options = ["--charset=UTF-8"]
  s.require_paths = ["lib"]
  s.required_ruby_version = Gem::Requirement.new(">= 1.9.3")
  s.rubygems_version = "1.8.23"
  s.summary = "A general classifier module to allow Bayesian and other types of classifications."

  if s.respond_to? :specification_version then
    s.specification_version = 2

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<fast-stemmer>, ["~> 1.0"])
      s.add_development_dependency(%q<minitest>, [">= 0"])
      s.add_development_dependency(%q<minitest-reporters>, [">= 0"])
      s.add_development_dependency(%q<pry>, [">= 0"])
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<rdoc>, [">= 0"])
      s.add_development_dependency(%q<redis>, [">= 0"])
      s.add_development_dependency(%q<rubocop>, [">= 0"])
    else
      s.add_dependency(%q<fast-stemmer>, ["~> 1.0"])
      s.add_dependency(%q<minitest>, [">= 0"])
      s.add_dependency(%q<minitest-reporters>, [">= 0"])
      s.add_dependency(%q<pry>, [">= 0"])
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<rdoc>, [">= 0"])
      s.add_dependency(%q<redis>, [">= 0"])
      s.add_dependency(%q<rubocop>, [">= 0"])
    end
  else
    s.add_dependency(%q<fast-stemmer>, ["~> 1.0"])
    s.add_dependency(%q<minitest>, [">= 0"])
    s.add_dependency(%q<minitest-reporters>, [">= 0"])
    s.add_dependency(%q<pry>, [">= 0"])
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<rdoc>, [">= 0"])
    s.add_dependency(%q<redis>, [">= 0"])
    s.add_dependency(%q<rubocop>, [">= 0"])
  end
end
